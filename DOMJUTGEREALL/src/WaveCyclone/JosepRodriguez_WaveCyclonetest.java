package WaveCyclone;

import static java.time.Duration.ofMillis;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


class JosepRodriguez_WaveCycloneTest {

	@Test
	public void testCalcul() {
		assertEquals("2",JosepRodriguez_WaveCyclone.calcul(45,1));
		assertEquals("3",JosepRodriguez_WaveCyclone.calcul(75,2));
		assertEquals("Has mort en el camp de mines",JosepRodriguez_WaveCyclone.calcul(15,2));
		assertEquals("0",JosepRodriguez_WaveCyclone.calcul(301,20));
		assertEquals("Has mort en el camp de mines",JosepRodriguez_WaveCyclone.calcul(-15,-1));
		//fail("Not yet implemented");
		//assertTimeoutPreemptively(ofMillis(1000), JosepRodriguez_WaveCyclone::move);
	}

}
